Document: maven-modules
Title: Debian maven-modules Manual
Author: <insert document author here>
Abstract: This manual describes what maven-modules is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/maven-modules/maven-modules.sgml.gz

Format: postscript
Files: /usr/share/doc/maven-modules/maven-modules.ps.gz

Format: text
Files: /usr/share/doc/maven-modules/maven-modules.text.gz

Format: HTML
Index: /usr/share/doc/maven-modules/html/index.html
Files: /usr/share/doc/maven-modules/html/*.html
